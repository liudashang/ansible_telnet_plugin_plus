# Ansible的telnet插件改进版

#### 介绍
Ansible 2.4版本以及之后的版本，新增加了对telnet的支持。但是结构简单，实际使用中用很多问题。我修改了代码，增强的代码的稳定性。

#### 软件架构
主要使用了python3，引入了re正则表达式模块。其他的与ansible官方源代码一直。


#### 安装教程

1. 通过ansibel-playbook --version或ansible --version命令，查找“ansible python module location”变量，确定ansible源码的位置。
2. 在ansible源码目录/plugins/action/下找到telnet.py文件。
3. 2.8版本的可以直接替换，其他版本请根据我的文件中的代码注释在对应位置添加代码。

#### 使用说明

1. 新增了enablepassword项，与user、password同级。主要给cisco设备使用，进入设置该项后telnet进入交换机会直接进入enable模式。
2. user项目，新增了“no-user”关键字。因为部分交换机telnet登录时会直接提示输入密码，跳过了用户名输入项，对于没有用户名的telnet，可以设置为user: no-user
3. 在command内增加了类似playbook when关键字的语法。原理是在代码中插入了re.compile(r'')正则表达式语句，使用方法看注释。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)